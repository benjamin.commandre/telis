#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, time
import configparser
import argparse

from app.Archive import Archive
import app.Constantes as Constantes

from app.Outils import Log

class Telechargement(object):

	def __init__(self, config):
		super(Telechargement, self).__init__()

		self.logger = Log("log", "Téléchargement")
		self.config = config

	def run(self):
		"""
			Fonction pour lancer le programme
		"""
		# Début du processus
		debut = time.time()

		prog = Archive(self.config)
		prog.listing()
		prog.download_auto()

		# Fin du processus
		fin = time.time()

		self.logger.info('Programme terminé en {} secondes'.format(fin - debut))
		nb_jours = int(time.strftime('%d', time.gmtime(fin - debut))) - 1
		self.logger.info("Cela représente {} jour(s) {}".format(nb_jours, time.strftime('%Hh %Mmin%S', time.gmtime(fin - debut))))

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-config", dest="config", help="Chemin du fichier de config", required=True)
	args = parser.parse_args()

	app = Telechargement(args.config)
	sys.exit(app.run())
