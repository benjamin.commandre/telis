#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import collections

SATELLITE = collections.defaultdict(dict)

# Informations obtenues sur le site 'Theia-land' ou sur le blog d'Olivier HAGOLLE

########################## SENTINEL2 ###############################

SATELLITE["SENTINEL2"]["serveur"] = "https://theia.cnes.fr/atdistrib"
SATELLITE["SENTINEL2"]["resto"] = "resto2"
SATELLITE["SENTINEL2"]["token_type"] = "text"
SATELLITE["SENTINEL2"]["R"] = 2
SATELLITE["SENTINEL2"]["PIR"] = 3
SATELLITE["SENTINEL2"]["LEVEL2A"] 	= ['_FRE_B2.tif', '_FRE_B3.tif', '_FRE_B4.tif', '_FRE_B8.tif']
SATELLITE["SENTINEL2"]["NUAGE"] 	= ['_CLM_R1.tif']
SATELLITE["SENTINEL2"]["LEVEL3A"] 	= ['_FRC_B2.tif', '_FRC_B3.tif', '_FRC_B4.tif', '_FRC_B8.tif']

########################## LANDSAT #################################

SATELLITE["Landsat"]["serveur"] = "https://theia-landsat.cnes.fr"
SATELLITE["Landsat"]["resto"] = "resto"
SATELLITE["Landsat"]["token_type"] = "json"
SATELLITE["Landsat"]["R"] = 3
SATELLITE["Landsat"]["PIR"] = 4

