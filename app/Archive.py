#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, glob, re, io
import math, subprocess
import urllib.request
import zipfile
import requests
import datetime
import configparser
from urllib.parse import urlencode

from osgeo import gdal, ogr, osr
import numpy as np
from uuid import uuid4

from collections import defaultdict, UserDict
import app.Outils as Outils
import app.Constantes as Constantes

id_tuile   = re.compile("T[0-9]+[A-Z]+")
date_tuile = re.compile("[SENTINEL.+?_]([0-9]*?)-")

class Archive():
    """
        Classe pour lister, télécharger les archives via site Theia selon un shapefile représentant la zone d'étude.

        :param capteur: Nom du satellite utilisé (ex: Landsat, Sentinel2, ...).
        :type capteur: Chaîne de caractères

        :param bandes: Bandes à conservées.
        :type bandes: Chaîne de caractères

        :param niveau: Niveau de traitement voulu.
        :type niveau: Chaîne de caractères

        :param emprise: Chemin vers le shapefile représentant l'emprise.
        :type emprise: Chaîne de caractères

        :param zone_etude: Chemin vers le shapefile représentant la zone d'étude.
        :type zone_etude: Chaîne de caractères

        :param sortie: Chemin du dossier de sortie.
        :type sortie: Chaîne de caractères

        :param annee_debut: Année à partir de laquelle on cherche les images.
        :type annee_debut: Entier

        :param annee_fin: Année jusqu'à laquelle on cherche les images.
        :type annee_fin: Entier

        :param seuil: Seuil maximal d'ennuagement.
        :type seuil: Entier
    """

    # def __init__(self, capteur, bandes, niveau, emprise, zone_etude, sortie, annee_debut, annee_fin, seuil):
    def __init__(self, config):
        """
            Créé une instance de la classe 'Archive'.
        """

        self.logger = Outils.Log("log", "Archive")

        Outils.get_variable(self, config)

        self.liste_archive = []
        self.url = ''

    def vector_projection_with_epsg(self, chemin_vecteur, output_path, epsg=32630):

        driver = ogr.GetDriverByName('ESRI Shapefile')

        projection_reference = osr.SpatialReference()
        projection_reference.ImportFromEPSG(epsg)

        shapefile = driver.Open(chemin_vecteur)
        layer = shapefile.GetLayer()
        projection_vector = layer.GetSpatialRef()

        transformation = osr.CoordinateTransformation(projection_vector, projection_reference)

        vecteur_reproj = output_path

        if os.path.exists(vecteur_reproj):
            driver.DeleteDataSource(vecteur_reproj)

        outDataSet = driver.CreateDataSource(vecteur_reproj)
        outLayer = outDataSet.CreateLayer(layer.GetName(), projection_reference, geom_type=layer.GetLayerDefn().GetGeomType())

        # add fields
        inLayerDefn = layer.GetLayerDefn()
        for i in range(0, inLayerDefn.GetFieldCount()):
            fieldDefn = inLayerDefn.GetFieldDefn(i)
            outLayer.CreateField(fieldDefn)

        outLayerDefn = outLayer.GetLayerDefn()

        inFeature = layer.GetNextFeature()

        while inFeature:
            # get the input geometry
            geom = inFeature.GetGeometryRef()
            # reproject the geometry
            geom.Transform(transformation)
            # create a new feature
            outFeature = ogr.Feature(outLayerDefn)
            # set the geometry and attribute
            outFeature.SetGeometry(geom)
            for i in range(0, outLayerDefn.GetFieldCount()):
                outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
            # add the feature to the shapefile
            outLayer.CreateFeature(outFeature)
            # dereference the features and get the next input feature
            outFeature = None
            inFeature = layer.GetNextFeature()

        # Save and close the shapefiles
        inDataSet = None
        outDataSet = None

    def coord_box_dd(self):
        """
            Méthode pour obtenir les coordonnées de la zone à partir du shapefile.

            :returns: Une chaîne de caractères représentant les coordonnées des coins situés \
            en bas à gauche et en haut à droite.
        """

        # Processus to convert the UTM shapefile
        # in decimal degrees shapefile with ogr2ogr in command line
        # utm_outfile = "{0}/UTM_{1}".format(os.path.split(self.emprise)[0], os.path.split(self.emprise)[1])

        # if os.path.exists(utm_outfile):
        #     os.remove(utm_outfile)

        # wgs84 : 4326
        # self.vector_projection_with_epsg(self.emprise, utm_outfile, epsg=4326) 

        # To get shapefile extent
        # Avec import ogr
        driver = ogr.GetDriverByName('ESRI Shapefile')
        # Open shapefile
        # data_source = driver.Open(utm_outfile, 0)
        data_source = driver.Open(self.emprise, 0)

        if data_source is None:
            self.logger.error('Could not open file')
            sys.exit(1)

        shp_ogr = data_source.GetLayer()
        # Extent
        extent_ = shp_ogr.GetExtent() # (xMin, xMax, yMin, yMax)

        ## Close source data
        data_source.Destroy()

        return "{},{},{},{}".format(extent_[0], extent_[2], extent_[1], extent_[3])

    def listing_by_tile(self):

        for tuile in self.liste_tuiles :

            self.requete['location'] = tuile

            self.url =  "{0}/{1}/api/collections/{2}/search.json?{3}".format(self.serveur, self.resto, self.capteur, urlencode(self.requete))

            self.get_liste_images()

    def listing_by_coord(self):

        self.requete['box'] = self.coord_box_dd()
        
        self.url =  "{0}/{1}/api/collections/{2}/search.json?{3}".format(self.serveur, self.resto, self.capteur, urlencode(self.requete))
        
        print(self.url)

        self.get_liste_images()

    def get_liste_images(self):
        
        # S'il existe une autre image, suivante = vrai, faux sinon
        suivante = True

        # Tant qu'il existe une image à traiter
        while suivante:

            try :
                request_headers = {"User-Agent": "Magic-browser"}
                req = urllib.request.Request(self.url, headers = request_headers) # Connexion à la base de données
                data = urllib.request.urlopen(req).read() # Lecture de la base de données

                new_data = re.sub(b"null", b"'null'", data) # Suppression de l'expression "null" pouvant causer une erreur Python
                new_data = re.sub(b"false", b"False", new_data) # Renomme le booléen selon la syntaxe Python

                # Conversion des données sous la forme d'un dictionnaire
                data_Dict = defaultdict(list)
                data_Dict = UserDict(eval(new_data))

                # Selection des archives à télécharger
                for d in range(len(data_Dict['features'])):
                    name_archive = data_Dict['features'][d]['properties']['productIdentifier']
                    feature_id = data_Dict["features"][d]['id']
                    link_archive = data_Dict['features'][d]['properties']['services']['download']['url'].replace("\\", "")
                    url_archive = link_archive.replace(self.resto, "rocket/#")
                    archive_download = url_archive.replace("/download", "") # Url de l'archive à télécharger
                    out_archive = "{0}/{1}.zip".format(self.dossier_sortie, name_archive) # Nom de sortie de l'archive

                    if "SENTINEL2X" not in out_archive or self.requete["processingLevel"] == "LEVEL3A":
                        self.liste_archive.append([archive_download, out_archive, feature_id])

                # Vérification si d'autres images sont disponibles
                for link in data_Dict['properties']['links'] :
                    if link["title"] is 'suivant' :
                        self.url = link['href'].replace("\\", "")
                        suivante = True
                        break
                    else :
                        suivante = False

            except Exception as e:
                self.logger.error("Error connexion or error variable : {0}".format(e))
                sys.exit(1)

    def listing(self):
        """
            Méthode pour lister les images disponibles sur la plateforme 'Theia Land' correspondant à la zone.
        """

        if hasattr(self, 'liste_tuiles'):
            self.listing_by_tile()
        else :
            self.listing_by_coord()           

        self.logger.info("{0} image(s) correspondent aux critères.".format(len(self.liste_archive)))

    def download_auto(self):
        """
            Méthode pour télécharger les archives sur le site Theia.

            :param identifiant: Identifant pour le site Theia.
            :type identifiant: Chaîne de caractères

            :param mdp: Mot de passe pour le site Theia.
            :type mdp: Chaîne de caractères

            :param proxy: Information du proxy.
            :type proxy: Chaîne de caractères
        """

        # Url pour obtenir l'authentification
        url = "{0}/services/authenticate/".format(self.serveur)

   
        # Dictionnaire contenant les informations du proxy s'il existe
        proxyDict = {
            "http"  : "{0}".format(self.proxy), \
            "https" : "{0}".format(self.proxy), \
            "ftp"   : "{0}".format(self.proxy) \
        }

        # Dictionnaire contenant les informations d'authentification
        payload = {
            'ident' : "{0}".format(self.id),\
            'pass' : "{0}".format(self.mdp)
        }

        # Requête pour obtenir le jeton d'identification
        reponse = requests.post(url, data=payload, proxies=proxyDict)

        # Récupération du jeton d'identification au format texte ou json
        try:
            if "text" in reponse.headers["Content-Type"] :
                token = reponse.text
            elif "json" in reponse.headers["Content-Type"] :
                token = reponse.json()["access_token"]
            else :
                raise(BaseException('Format non traité.'))
        except Exception as e:
            self.logger.error("Error during the identification request")
            sys.exit(-1)

        head = {"Authorization": "Bearer {0}".format(token)}

        # Dictionnaire des images à traiter regrouper par date
        dico_date = dict()

        sauvegarde = configparser.ConfigParser()

        # Pour toutes les images disponible
        for l in self.liste_archive :

            # Récupération de la date quand l'image a été prise
            date = date_tuile.search(l[1]).group(1)

            # Récupération du nom de l'image
            nom_image = l[1].split("/")[-1][:-4]

            # Lecture du fichier de sauvegarde
            sauvegarde.read("{}/sauvegarde.ini".format(self.dossier_sortie))

            # Si la date existe dans le fichier de sauvegarde ...
            if str(date) in sauvegarde.keys() :

                # ... mais pas l'image, on la rajoute dans la liste des images à traiter.
                if nom_image not in sauvegarde[str(date)].keys() :

                    sauvegarde[str(date)][nom_image] = "False"

                    if date in dico_date.keys() :
                        dico_date[date].append(l)
                    else :
                        dico_date[date] = [l]

                # ... et que l'image exite mais n'a pas été traité, on la rajoute dans la liste des images à traiter.
                elif sauvegarde[str(date)][nom_image] == "False" :

                    if date in dico_date.keys() :
                        dico_date[date].append(l)
                    else :
                        dico_date[date] = [l]

            # Si la date n'existe pas dans le fichier de sauvegarde, on la rajoute ainsi que l'image dans la liste des images à traiter.
            else :
                sauvegarde[str(date)] = {}
                sauvegarde[str(date)][nom_image] = "False"

                if date in dico_date.keys() :
                    dico_date[date].append(l)
                else :
                    dico_date[date] = [l]

            # Sauvegarde du fichier de sauvegarde mis à jour.
            with open("{}/sauvegarde.ini".format(self.dossier_sortie), 'w') as configfile:
                sauvegarde.write(configfile)

        # Pour toutes les dates à traiter
        for cle in sorted(dico_date):
            # Liste des archives
            liste_content = []

            # Pour toutes les images prisent à cette date
            for idx, img in enumerate(dico_date[cle]):

                self.logger.info("Requête pour l'archive : {0}".format(img[1].split("/")[-1]))
                # Url de l'archive
                url = "{0}/{1}/collections/{2}/{3}/download/?issuerId=theia".format(self.serveur, self.resto, self.capteur, img[2])
                self.logger.debug("url : {}".format(url))

                # Requête pour récupérer l'archive
                reponse = requests.get(url, headers=head, proxies=proxyDict)

                if self.extraction :
                     # Ajout de l'archive à la liste
                    liste_content.append(reponse.content)
                    del reponse
                else :
                    if self.groupe == "Tuile" :
                        dossier_archive = "{0}/{1}".format(self.dossier_sortie, id_tuile.search(img[1]).group(0))
                    else:
                        dossier_archive = "{0}/Archive/{1}".format(self.dossier_sortie, cle[:4])

                    if not os.path.exists(dossier_archive):
                        os.makedirs(dossier_archive)

                    print("{0}/{1}".format(dossier_archive, img[1].split("/")[-1]))
                    with open("{0}/{1}".format(dossier_archive, img[1].split("/")[-1]), "wb") as fichier:
                        fichier.write(reponse.content)

            if self.extraction :
                # Traitement des images (fusion, découpage selon la zone d'étude ...)
                self.traitement_images(cle, liste_content)
                del liste_content


            # Mis à jour du fichier de sauvegarde
            # Lecture du fichier de sauvegarde
            sauvegarde.read("{}/sauvegarde.ini".format(self.dossier_sortie))

            # Pour toutes les images traitées à cette date
            for img in dico_date[cle] :
                # Image traitée passe à vrai dans le fichier de sauvegarde
                sauvegarde[str(cle)][img[1].split("/")[-1][:-4]] = "True"

            # Sauvegarde du fichier de sauvegarde mis à jour.
            with open("{}/sauvegarde.ini".format(self.dossier_sortie), 'w') as configfile:
                sauvegarde.write(configfile)

        self.logger.info("All images have been downloaded !")

    def calcul_ennuagement(self, tuiles_nuage):

        option_warp     = gdal.WarpOptions(format='Mem', cropToCutline=True, outputType=gdal.GDT_Float32,\
        								   cutlineDSName=self.emprise, dstNodata="NaN")
        mosaic_nuage    = gdal.Warp("", tuiles_nuage, options=option_warp)

        rows = mosaic_nuage.RasterYSize # Rows number
        cols = mosaic_nuage.RasterXSize # Columns number

        data = mosaic_nuage.GetRasterBand(1).ReadAsArray(0, 0, cols, rows)

        nombre_pixel_zone = 0
        nombre_pixel_null = 0
        nombre_pixel_non_nuage = 0

        for donnees in data :
            d = donnees.tolist()
            nombre_pixel_zone += len(d)
            nombre_pixel_null += d.count(-10000) + np.isnan(d).sum()
            nombre_pixel_non_nuage += d.count(0)

        nombre_pixel_non_null = nombre_pixel_zone - nombre_pixel_null

        del data

        ennuagement = (float(nombre_pixel_non_nuage)/float(nombre_pixel_non_null)) if nombre_pixel_non_null != 0 else 0.0
        self.logger.debug("Ennuagement : {}%".format(100 - round(ennuagement*100, 2)))

        # Computer cloud's percentage with dist (sum of cloud) by sum of the image's extent
        return 1.0 - ennuagement

    def calcul_aire(self, tuiles_image):
        """
            Méthode pour fusionner un ensemble d'images et calculer l'aire de l'image obtenue.

            :param tuiles_image: Liste des images à fusionner.
            :type tuiles_image: Liste d'images

            :returns: Un tuple contenant l'image fusionnée ainsi que son aire.
        """

        option_warp     = gdal.WarpOptions(format='Mem', resampleAlg="lanczos")
        mosaic_image    = gdal.Warp("", tuiles_image, options=option_warp)

        rows = mosaic_image.RasterYSize # Rows number
        cols = mosaic_image.RasterXSize # Columns number

        data        = mosaic_image.GetRasterBand(1).ReadAsArray(0, 0, cols, rows).astype(np.float16)
        mask_spec   = ~(np.isnan(data) | np.isin(data, [-10000]))

        area = float((np.sum(mask_spec) * mosaic_image.GetGeoTransform()[1] * abs(mosaic_image.GetGeoTransform()[-1]) )/10000)

        return mosaic_image, area

    def ecriture_geotiff(self, dataset, chemin):
        """
            Méthode pour enregistrer une image au format GTiff.

            :param dataset: Données de l'image.
            :type dataset: GDALDataset

            :param chemin: Chemin de l'image de sortie.
            :type chemin: Chaîne de caractères
        """
        gdal.AllRegister()

        driver = gdal.GetDriverByName('GTiff')
        outdata = driver.Create(chemin, dataset.RasterXSize, dataset.RasterYSize, dataset.RasterCount, gdal.GDT_Float32)
        outdata.SetGeoTransform(dataset.GetGeoTransform())
        outdata.SetProjection(dataset.GetProjection())

        for band in range(dataset.RasterCount) :
            data = dataset.GetRasterBand(band + 1).ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize).astype(np.float32)
            outdata.GetRasterBand(band + 1).WriteArray(data, 0, 0)
            outdata.GetRasterBand(band + 1).FlushCache()

        outdata = None

    def traitement_images(self, date, liste_content):
        """
            Pour une date donnée, extrait pour chaque archive correspondant à cette date
            les images correspondant aux bandes rouge, verte, bleue et proche infrarouge,
            les combines en une seule image, effectue la mosaïque des images,
            coupe selon l'emprise de la zone d'étude puis, si l'aire de l'image est supérieure à 0 l'enregistre.

            :param date: Date à laquelle ont été prises les images.
            :type date: Entier

            :param liste_content: Liste des archives contenant les images.
            :type liste_content: Liste d'octets

        """

        self.logger.info("Date : {0} ".format(date))

        tuiles_image = []
        tuiles_nuage = []

        # Options Gdal pour la fusion des bandes
        options_vrt = gdal.ParseCommandLine('-resolution highest -srcnodata -10000 -vrtnodata NaN -separate')

        self.logger.info("Extraction des images")

        # Pour chaque archive
        for idx, content in enumerate(liste_content) :

            # Lecture de l'archive
            tzip = zipfile.ZipFile(io.BytesIO(content))

            # Liste des fichiers dans l'archive
            zip_img = tzip.namelist()

            # Liste contenant les images voulues
            liste_bandes = []

            liste_mem = []
            # Pour tous les fichiers dans l'archive
            for id_ext, extension in enumerate(self.extent_img) :
                # Si il s'agit d'une bande voulue (R,G,B,PIR)
                img = [f for f in zip_img if extension in f][0]

                # On charge l'image en mémoire
                mmap_name = "/vsimem/"+uuid4().hex
                liste_mem.append(mmap_name)
                gdal.FileFromMemBuffer(mmap_name, tzip.read(img))
                # liste_bandes.append(gdal.Open(mmap_name))
                liste_bandes.append(mmap_name)

            # On fusionne les différentes bandes en une seule image
            # on découpe l'image selon l'emprise
            # et on conserve l'image obtenue
            vrt = gdal.BuildVRT("", liste_bandes, options=options_vrt)

            image = Outils.clip(vrt, self.zone_etude)
            _, aire = self.calcul_aire(image)

            del image

            if aire > 0.0 :
                tuiles_image.append(vrt)

                # On libère la mémoire
                for mmap_name in liste_mem :
                    gdal.Unlink(mmap_name)

                liste_mem = []

                if self.nuage is not None :
                    image_nuage = [f for f in zip_img if self.nuage[0] in f][0]
                    mmap_name = "/vsimem/"+uuid4().hex

                    gdal.FileFromMemBuffer(mmap_name, tzip.read(image_nuage))
                    liste_mem.append(mmap_name)
                    tuiles_nuage.append(Outils.clip(mmap_name, self.zone_etude))

            for mmap_name in liste_mem :
                gdal.Unlink(mmap_name)

            liste_mem = []

            liste_bandes = None

            del tzip
            del liste_content[idx]

        del liste_content

        if len(tuiles_image) > 0 :

            if len(tuiles_nuage) == 0 or self.calcul_ennuagement(tuiles_nuage) <= self.seuil_nuage:

                self.logger.info("Sauvegarde des images")

                dossier = "{0}/Images/{1}".format(self.dossier_sortie, date[:4])

                if not os.path.exists(dossier):
                    os.makedirs(dossier)

                self.logger.debug("Dossier image : {0}".format(dossier))

                # On effectue une mosaïque des images qu'on découpe selon l'emprise.
                Outils.clip(tuiles_image, self.emprise, form="GTiff", dst="{0}/{1}.tif".format(dossier,date))

                del tuiles_nuage

            del tuiles_image
