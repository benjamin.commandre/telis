#!/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
import logging
from logging.handlers import RotatingFileHandler
import signal
from contextlib import contextmanager
import inspect
from osgeo import gdal
import app.Constantes as Constantes
import configparser
import ast
from datetime import datetime
import app.Satellites as Satellites

class Fusion(argparse.Action):
	def __init__(self, option_strings, dest, nargs=None, **kwargs):
		super(Fusion, self).__init__(option_strings, dest, nargs, **kwargs)
	def __call__(self, parser, namespace, values, option_string=None):
		setattr(namespace, self.dest, ' '.join(values).lower())

class Log(object):

	def __init__(self, dossier, nomFichier, niveau=None):

		super(Log, self).__init__()

		stack = inspect.stack()
		nom_classe = stack[1][0].f_locals["self"].__class__.__name__

		self.__logger__ = logging.getLogger(nomFichier)

		if niveau is None:
			try:
				niveau = Constantes.NIVEAU_DEFAUT
			except :
				niveau = logging.DEBUG

		self.__logger__.setLevel(niveau)

		format_fichier = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
		fichierLog = RotatingFileHandler("{0}/{1}.log".format(dossier, nomFichier), 'a', 1000000, 1)

		fichierLog.setLevel(niveau)
		fichierLog.setFormatter(format_fichier)
		self.__logger__.addHandler(fichierLog)

		format_console = logging.Formatter('%(asctime)s :: {0} :: %(levelname)s :: %(message)s'.format(nom_classe), datefmt='%d-%m-%Y %H:%M:%S')

		console = logging.StreamHandler()
		console.setLevel(niveau)
		console.setFormatter(format_console)
		self.__logger__.addHandler(console)

	def info(self, message):
		self.__logger__.info(message)

	def debug(self, message):
		self.__logger__.debug(message)

	def warning(self, message):
		self.__logger__.warning(message)

	def error(self, message):
		self.__logger__.error(message)

	def critical(self, message):
		self.__logger__.critical(message)

	def exception(self, message):
		self.__logger__.exception(message)

	def close(self):
		for handler in  self.__logger__.handlers[:] :
			handler.close()
			self.__logger__.removeHandler(handler)


class TimeoutException(Exception): pass

@contextmanager
def limitation_temporelle(secondes):

	def signal_handler(signum, frame):
		raise TimeoutException

	signal.signal(signal.SIGALRM, signal_handler)
	signal.alarm(secondes)

	try:
		yield
	finally:
		signal.alarm(0)

# Utilisation :
#
# try:
# 	with limitation_temporelle(temps_en_seconde):
# 		appel_fonction()
# except TimeoutException:
# 	pass

def clip(image, cut, form="Mem", dst="", type_sortie=gdal.GDT_Float32, nodata="NaN"):
	"""
		Découpe une image selon un shapefile donné. Si le type de l'image en sortie
		n'est pas spécifié, l'image sera retournée en pointeur mémoire et ne sera pas sauvegardée.

		:param image: Chemin vers l'image dans laquelle on veut découper l'emprise.
		:type image: Chaîne de caractères

		:param cut: Chemin vers le shapefile représentant l'emprise.
		:type cut: Chaîne de caractères

		:param form: Format de l'image en sortie.
		:type form: Chaîne de caractères

		:param dst: Chemin de l'image en sortie.
		:type dst: Chaîne de caractères
	"""

	option_clip = gdal.WarpOptions(cropToCutline=True,\
	cutlineDSName=cut, outputType=type_sortie , format=form, dstNodata=nodata)

	return gdal.Warp(dst, image, options=option_clip)

def str2bool(v):
    return v.lower() in (["false"])

def checkDate(date):

	d = date.split('-')

	try:
		if not date :

			annee = datetime.now().year
			
			if datetime.now().month < 10 :
				mois  = "0{}".format(datetime.now().month)
			else :
				mois = datetime.now().month

			if datetime.now().day < 10 :
				jour  = "0{}".format(datetime.now().day)
			else :
				jour  = datetime.now().day
			
		elif len(d)  == 1 :
			annee = d[0]
			mois = "01"
			jour =  "01"
		elif len(d) == 2 :
			annee = d[0]
			mois =  d[1]
			jour =  "01"
		elif len(d) == 3 :
			annee = d[0]
			mois =  d[1]
			jour =  d[2]

		datetime(int(annee), int(mois), int(jour))
		return "{annee}-{mois}-{jour}".format(annee=annee, mois=mois, jour=jour)
	except ValueError:
		print("Format invalide")
		sys.exit(1)
	except IndexError:
		print("La date doit être au format 'AAAA-MM-JJ', 'AAAA-MM' ou 'AAAA'")
		sys.exit(1)


def get_variable(self, config):
	"""
		Récupération des variables dans le fichier de configuration
	"""

	configfile = configparser.ConfigParser()
	configfile.read(config)

	self.requete = dict()
	self.requete["lang"] = "fr"
	self.requete["_pretty"] = "true"
	self.requete["maxRecord"] = 500

	# Capteur utilisé
	self.capteur = configfile["satellite"]["capteur"]
	self.bandes  = configfile["satellite"]["bandes"]
	self.requete["processingLevel"] = configfile["satellite"]["processingLevel"]

	# Dossier contenant les résultats
	self.dossier_sortie = configfile["sortie"]["chemin"]

	try:
		if str2bool(configfile["sortie"]["extraction"]):
			self.extraction = False
		else :
			self.extraction = True
	except :
		self.extraction = True

	self.groupe = configfile["sortie"]["groupe"]

	# Date de début et de fin de la recherche
	try:
		# self.date_debut = configfile["donnees"]["date_debut"]
		self.requete["startDate"] = checkDate(configfile["donnees"]["date_debut"])
	except Exception as e:
		raise "L'année de départ est requise."

	self.requete["completionDate"] = checkDate(configfile["donnees"]["date_fin"])

	self.seuil_nuage = float(configfile["donnees"]["seuil_nuage"])/100.0 if configfile["donnees"]["seuil_nuage"] else 0.0

	# Emprise et zone de l'étude
	self.emprise    = configfile["donnees"]["chemin_emprise"]

	if self.emprise :
		self.zone_etude = configfile["donnees"]["chemin_zone_etude"]
		if not self.zone_etude :
			self.zone_etude = self.emprise
	else :
		try:
			self.liste_tuiles = list(ast.literal_eval(configfile["donnees"]["liste_tuiles"]))
		except Exception as e:
			raise "Aucune emprise ni aucune tuile n'a été renseigné."
		

	self.nombre_image = configfile["donnees"]["nombre_image"]

	# Identifiant, mot de passe et proxy pour le téléchargement des images Théia
	self.id 	= configfile["theia"]["identifiant"]
	self.mdp 	= configfile["theia"]["mdp"]
	self.proxy 	= configfile["theia"]["proxy"]

	if self.bandes == "RGB" :
		self.extent_img = Satellites.SATELLITE[self.capteur][self.requete["processingLevel"]][:-1]
	else :
		self.extent_img = Satellites.SATELLITE[self.capteur][self.requete["processingLevel"]]

	if self.requete["processingLevel"] == "LEVEL2A" :
		self.nuage = Satellites.SATELLITE[self.capteur]["NUAGE"]
	else :
		self.nuage = None

	self.serveur = Satellites.SATELLITE[self.capteur]["serveur"]
	self.resto = Satellites.SATELLITE[self.capteur]["resto"]
	self.token_type = Satellites.SATELLITE[self.capteur]["token_type"]
